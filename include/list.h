/* MIT License
 *  
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh0@protonmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef LIST_H
#define LIST_H

#include <stddef.h>
#include <stdint.h>

#define LIST_DEFAULT_CAPACITY 16
#define LIST_DEFAULT_RESIZE_FACTOR 2
#define LIST_AT(list, i, type) ((type *)(list)->arr)[i]

struct list {
	// Size of each element.
	size_t element_size;

	// Number of elements in the list.
	size_t size;
	// Number of elements that can be stored in the list.
	size_t capacity;

	// The factor for list growth when a resize is needed.
	int resize_factor;

	// Storage for the elements.
	void *arr;

	// If set, this pointer will be used when internal operations
	// need to compare two elements.
	// If not set, memcmp will be used instead.
	//
	// Feel free to edit this directly at anytime.
	int (*cmp_fn)(const void *, const void *);
};

/**
 * @brief Initializes the list.
 *
 * @param list Pointer to the list that will be initialized.
 * @param element_size Size, in bytes, of each element.
 * @param initial_capacity Number of elements allocated at first.
 *
 * @return int
 * 	 0 -> Success
 * 	-1 -> Error
 */
int list_init(struct list *list, size_t element_size);

/**
 * @brief Pushes, by copying, a value to the first position of the list.
 *
 * @param list Pointer to the list.
 * @param value Bytes of the value will be pushed into the list.
 *
 * @return int
 *    	 0 -> Success
 * 	-1 -> Error
 */
int list_push_front(struct list *restrict list, const void *restrict value);

/**
 * @brief Pushes, by copying, a value to the last position of the list.
 *
 * @param list Pointer to the list.
 * @param value Bytes of the value will be pushed into the list.
 *
 * @return int
 * 	 0 -> Success
 * 	-1 -> Error
 */
int list_push_back(struct list *restrict list, const void *restrict value);

/**
 * @brief Inserts, by copying, a value at the index position of the list.
 *
 * @param list Pointer to the list.
 * @param value Bytes of the value will be inserted into the list.
 * @param index Position at which the value will be inserted.
 * If 0, this functions equals list_push_front, if size, this function
 * equals list_push_back.
 *
 * @return int
 * 	 0 -> Success
 * 	-1 -> Error
 */
int list_insert(struct list *restrict list, const void *restrict value,
		size_t index);

/**
 * @brief Pops a value from the last position of the list.
 *
 * @param list Pointer to the list.
 * @param value Buffer where the bytes of the element will be copied into. If
 * NULL, pop stills succeds, but without returning the user a copy of the value.
 *
 * @return int
 * 	 0 -> Success
 * 	-1 -> Error
 */
int list_pop_front(struct list *restrict list, void *restrict value);

/**
 * @brief Pops a value from the last position of the list.
 *
 * @param list Pointer to the list.
 * @param value Buffer where the bytes of the element will be copied into. If
 * NULL, pop stills succeds, but without returning the user a copy of the value.
 *
 * @return int
 * 	 0 -> Success
 * 	-1 -> Error
 */
int list_pop_back(struct list *restrict list, void *restrict value);

/**
 * @brief Removes, by copying, a value at the index position of the list.
 *
 * @param list Pointer to the list.
 * @param value Buffer where the bytes of the element will be copied into. If
 * NULL, pop stills succeds, but without returning the user a copy of the value.
 * @param index Position from which the element will be removed.
 * If 0, this functions equals list_pop_front, if size, this function
 * equals list_pop_back.
 *
 * @return int
 * 	 0 -> Success
 * 	-1 -> Error
 */
int list_remove(struct list *restrict list, void *restrict value, size_t index);

/**
 * @brief Destroys a list by resetting it and deallocating the memory used.
 * list_init must be called again.
 *
 * @param list Pointer to the list.
 */
void list_destroy(struct list *list);

/**
 * @brief Checks if the list contains the specified value.
 *
 * @param list Pointer to the list.
 * @param value Value that'll be checked.
 *
 * @return uint8_t
 * 	0 -> false
 * 	1 -> true
 */
uint8_t list_contains(struct list *restrict list, const void *restrict value);

#endif
