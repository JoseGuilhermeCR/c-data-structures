/* MIT License
 *  
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh0@protonmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "list.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

static int list_resize(struct list *list);

int list_init(struct list *list, size_t element_size)
{
	assert(list);
	assert(element_size != 0);

	list->element_size = element_size;

	list->size = 0;

	list->capacity = LIST_DEFAULT_CAPACITY;
	list->resize_factor = LIST_DEFAULT_RESIZE_FACTOR;

	list->arr = calloc(list->capacity, list->element_size);
	list->cmp_fn = NULL;

	if (!list->arr)
		return -1;

	return 0;
}

int list_push_front(struct list *restrict list, const void *restrict value)
{
	assert(list);
	assert(value);

	if (list->size == list->capacity && list_resize(list) != 0)
		return -1;

	memmove(list->arr + list->element_size, list->arr,
		list->size * list->element_size);
	memcpy(list->arr, value, list->element_size);

	++list->size;

	return 0;
}

int list_push_back(struct list *restrict list, const void *restrict value)
{
	assert(list);
	assert(value);

	if (list->size == list->capacity && list_resize(list) != 0)
		return -1;

	memcpy(list->arr + (list->size * list->element_size), value,
	       list->element_size);
	++list->size;

	return 0;
}

int list_insert(struct list *restrict list, const void *restrict value,
		size_t index)
{
	assert(list);
	assert(value);

	if (index > list->size)
		return -1;

	if (index == 0)
		return list_push_front(list, value);
	else if (index == list->size)
		return list_push_back(list, value);

	if (list->size == list->capacity && list_resize(list) != 0)
		return -1;

	memmove(list->arr + index * list->element_size + list->element_size,
		list->arr + index * list->element_size,
		(list->size - index) * list->element_size);
	memcpy(list->arr + index * list->element_size, value,
	       list->element_size);
	++list->size;

	return 0;
}

int list_pop_front(struct list *restrict list, void *restrict value)
{
	assert(list);
	if (list->size == 0)
		return -1;

	if (value)
		memcpy(value, list->arr, list->element_size);

	memcpy(list->arr, list->arr + list->element_size,
	       list->size * list->element_size);
	--list->size;

	return 0;
}

int list_pop_back(struct list *restrict list, void *restrict value)
{
	assert(list);
	if (list->size == 0)
		return -1;

	--list->size;
	if (value)
		memcpy(value, list->arr + (list->size * list->element_size),
		       list->element_size);

	return 0;
}

int list_remove(struct list *restrict list, void *restrict value, size_t index)
{
	assert(list);

	if (index > list->size)
		return -1;

	if (index == 0)
		return list_pop_front(list, value);
	else if (index == list->size)
		return list_pop_back(list, value);

	if (value)
		memcpy(value, list->arr + index * list->element_size,
		       list->element_size);

	--list->size;
	memcpy(list->arr + index * list->element_size,
	       list->arr + index * list->element_size + list->element_size,
	       (list->size - index) * list->element_size);

	return 0;
}

void list_destroy(struct list *list)
{
	free(list->arr);
	list->arr = NULL;
	list->size = 0;
	list->capacity = 0;
	list->element_size = 0;
}

uint8_t list_contains(struct list *restrict list, const void *restrict value)
{
	assert(list);
	assert(value);

	for (size_t i = 0; i != list->size; ++i) {
		int result;

		if (!list->cmp_fn) {
			result = memcmp(list->arr + list->element_size * i,
					value, list->element_size);
		} else {
			result = list->cmp_fn(
				list->arr + list->element_size * i, value);
		}

		if (result == 0) {
			return 1;
		}
	}

	return 0;
}

static int list_resize(struct list *list)
{
	list->capacity *= LIST_DEFAULT_RESIZE_FACTOR;

	void *new = calloc(list->capacity, list->element_size);
	if (!new) {
		list->capacity /= LIST_DEFAULT_RESIZE_FACTOR;
		return -1;
	}

	memcpy(new, list->arr, list->size * list->element_size);
	free(list->arr);
	list->arr = new;

	return 0;
}

// Usage Example:
//
//
//struct MyStruct {
//	int a;
//	int b;
//};
//
//int main()
//{
//	struct list list;
//
//	list_init(&list, sizeof(struct MyStruct));
//
//	struct MyStruct mstruct = { .a = 20, .b = 3 };
//
//	list_push_back(&list, (void *)&mstruct);
//	list_push_back(&list, (void *)&mstruct);
//	list_push_back(&list, (void *)&mstruct);
//
//	puts("Original List: ");
//	for (size_t u = 0; u != list.size; ++u) {
//		printf("%u %u\n", LIST_AT(&list, u, struct MyStruct).a,
//		       LIST_AT(&list, u, struct MyStruct).b);
//		LIST_AT(&list, u, struct MyStruct).a *= 3;
//		LIST_AT(&list, u, struct MyStruct).b *= 4;
//	}
//
//	puts("Modified List: ");
//	for (size_t u = 0; u != list.size; ++u) {
//		printf("%u %u\n", LIST_AT(&list, u, struct MyStruct).a,
//		       LIST_AT(&list, u, struct MyStruct).b);
//	}
//
//	list_pop_back(&list, NULL);
//	list_pop_back(&list, NULL);
//	list_pop_back(&list, NULL);
//
//	puts("Final List: ");
//	for (size_t u = 0; u != list.size; ++u) {
//		printf("%u %u\n", LIST_AT(&list, u, struct MyStruct).a,
//		       LIST_AT(&list, u, struct MyStruct).b);
//	}
//
//	list_destroy(&list);
//	return 0;
//}
